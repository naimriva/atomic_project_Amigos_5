-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2017 at 05:25 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b44`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(1, 'rrr', 'rivaftf', 'No'),
(2, 'rrr', 'riva', 'Yes'),
(3, 'naim', 'bb', 'No'),
(4, 'himu', 'humayun Ahmed', 'No'),
(5, 'nn', 'rr', 'No'),
(8, 'gyliver', 'mr. x', 'No'),
(9, 'à¦…à¦®à¦¨à¦¿à¦¬à¦¾à¦¸', 'à¦†à¦°à§à¦¥à¦¾à¦° à¦•à§‹à¦¨à¦¾à¦¨', 'No'),
(10, 'science friction', 'mr. Y', 'No'),
(11, 'error', 'riva', 'No'),
(12, 'image processing', 'Mk', 'No'),
(13, 'naima', 'nn', 'No'),
(14, 'mrr', 'x', 'No'),
(15, 'hello world', 'naim', 'No'),
(16, 'b', 'x', 'No'),
(17, 'nnnnnnn', 'huhg', 'No'),
(18, 'aaa', 'y', 'No'),
(19, 'nmm', 'naima', 'No'),
(20, 'nmm', 'naima', 'No'),
(21, 'jannat', 'mr.z', 'No'),
(22, 'himu', 'humaun hmed ', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `soft_delete`) VALUES
(1, 'riva', 'Dhaka', 'No'),
(2, 'Nazmun', 'Chittagong', 'No'),
(3, 'moniya', 'Chittagong', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `dob`
--

CREATE TABLE `dob` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dob`
--

INSERT INTO `dob` (`id`, `name`, `date`, `soft_delete`) VALUES
(1, 'rrrr', '1993-04-04', 'n'),
(2, 'nazmun', '2017-11-03', 'n'),
(3, 'kash', '2011-10-31', 'n'),
(4, 'poca', '2001-10-02', 'n'),
(5, 'kash', '1992-05-18', 'No'),
(6, 'saima', '1994-02-24', 'No'),
(9, 'kashfia', '1992-12-18', 'No'),
(11, 'fhf', '1234-03-03', 'No'),
(12, 'moniya', '1990-03-03', 'No'),
(13, 'riva', '1234-03-03', 'No'),
(14, 'eva', '1993-05-03', 'No'),
(15, 'sampoo', '1990-03-03', 'No'),
(16, 'naima', '1234-03-03', 'No'),
(17, 'Rivanaim', '1990-03-03', 'No'),
(18, 'fhf1', '1234-03-03', 'No'),
(19, 'fhf1', '1234-03-03', 'No'),
(20, 'bbb', '8899-06-07', 'No'),
(21, 'mmm', '9999-01-09', 'No'),
(22, 'mou', '2017-02-16', 'No'),
(23, 'mamila', '2017-02-24', 'No'),
(24, 'dsfsdc', '2015-11-30', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_delete`) VALUES
(1, '', '', 'Yes'),
(2, 'jgjhg', 'sdlkfjk@sdkjf.com', 'No'),
(3, '', 'riva.naim25@gmail.com', 'Yes'),
(4, 'kash', 'kash.sanju92@gmail.com', 'No'),
(5, 'naim', 'naim@gmail.com', 'No'),
(6, '', '', 'Yes'),
(7, 'hhhh', 'hh.@hhhh', 'Yes'),
(8, 'smapoo', 'sam@gmail.com', 'No'),
(9, 'Farhan', 'famouctg@gmail.com', 'No'),
(10, 'saima', 'sss@hma', 'No'),
(11, 'sampoo', 'ssd', 'No'),
(12, 'sdce', 'vfvfb', 'No'),
(13, 'ashgbnx', 'gyhbn', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `soft_delete`) VALUES
(1, 'naim', 'Female', 'No'),
(2, 'saima', 'Female', 'No'),
(3, '', 'Female', 'No'),
(4, '', 'Female', 'No'),
(5, '', 'Female', 'No'),
(6, 'rasel', 'Male', 'No'),
(7, '', 'Female', 'No'),
(8, 'benu', 'Female', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobby`, `soft_delete`) VALUES
(1, 'riva', 'Photography,Watch Movie,Reading Book', 'No'),
(2, 'saima', 'Gardening,Watch Movie,Reading Book', 'No'),
(3, 'Riva', 'Gardening,Watch Movie', 'No'),
(4, 'Nazmun', 'Photography,Watch Movie', 'No'),
(5, 'naim', 'Photography,Gardening', 'No'),
(6, 'jannat', 'Watch Movie,Reading Book', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `org_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `name`, `org_name`, `summary`, `soft_delete`) VALUES
(1, 'naim', 'CClub', 'jinga laa laa', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pic`
--

CREATE TABLE `profile_pic` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_pic`
--

INSERT INTO `profile_pic` (`id`, `name`, `image`, `soft_delete`) VALUES
(1, 'eiva', '148618661311665466_508759059310025_7927952105731826119_n.jpg', 'No'),
(2, 'tttt', '148618675616179771_1689149891111579_8393453934653083457_o.jpg', 'No'),
(3, 'sammu', '148653565216178945_1689139764445925_3786815026579171930_o.jpg', 'No'),
(4, 'hhhh', '148679387711665466_508759059310025_7927952105731826119_n.jpg', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dob`
--
ALTER TABLE `dob`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_pic`
--
ALTER TABLE `profile_pic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dob`
--
ALTER TABLE `dob`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `profile_pic`
--
ALTER TABLE `profile_pic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
