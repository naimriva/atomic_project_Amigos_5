<?php

require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)) session_start();

$msg= Message::getMessage();

if($msg) {

    echo "<div>  <div id='message'>  $msg </div>   </div>";

    $_SESSION['message'] = "";
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">

    <style>
        body{
            background: antiquewhite;
        }
    </style>
</head>
<body>


<div class="container">
    <form action="store.php" method="post">

        <h1 style="color: #442a8d;">Add City Form</h1>

        <strong> Enter Your Name:</strong>
        <input type="text" name="name">
        <br>
        <div class="select-style">
            <select name="city_name">
                <option selected>Select Any One</option>
                <option value="Dhaka">Dhaka</option>
                <option value="Chittagong" >Chittagong</option>
                <option value="Khulna" >Khulna</option>
                <option value="Barishal">Barishal</option>
                <option value="Sylhet" >Sylhet</option>
            </select>
        </div>
        <input type="submit" class="btn btn-primary" value="SUBMIT">

    </form>




</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>

    jQuery (function($){

        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>
</body>
</html>

