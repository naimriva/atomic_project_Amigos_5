<?php
require_once("../../../vendor/autoload.php");

$objEmail = new \App\Email\Email();

$allData = $objEmail->trashed();

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div style='height: 30px'> <div  id='message'> $msg </div> </div>";

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email - Trashed List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
            background: antiquewhite;
        }

        body{
            background: antiquewhite;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
    </style>



</head>
<body>
<div class="container">

    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>

    <div class="navbar">
        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active List</a> </td>




    <h1 style="text-align: center" ;">email - Trashed List</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>Serial Number</th>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Action Buttons</th>
        </tr>

        <?php
        $serial= 1;
        foreach($allData as $oneData){

            if($serial%2) $bgColor = "#cccccc";
            else $bgColor = "#ffffff";

            echo "

                  <tr  style='background-color: $bgColor'>
                     <td style='width: 10%; text-align: center'>$serial</td>
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->email</td>

                     <td><a href='view.php?id=$oneData->id' class='btn btn-info'>View</a> </td>
                  </tr>
              ";
            $serial++;
        }
        ?>

    </table>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>