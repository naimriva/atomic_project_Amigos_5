<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div id='message'> $msg </div>";



$objBirthday = new \App\Birthday\Birthday();
$objBirthday->setData($_GET);
$oneData = $objBirthday->view();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birthday edit Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
    <style>
        body{
            background: antiquewhite;
        }
    </style>

</head>
<body>

<div class="container">

    <form  class="form-group" action="update.php" method="post">

        Enter your Name:
        <input class="form-control" type="text" name="name" value="<?php echo $oneData->name ?>">
        <br>
        Date of Birth:
        <input class="form-control" type="date" name="date"  value="<?php echo $oneData->date ?>">
        <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
        <input type="submit">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


