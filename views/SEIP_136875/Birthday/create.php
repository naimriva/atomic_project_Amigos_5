<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birthday Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
<style>
    body{
        background: antiquewhite;
    }
</style>
</head>
<body>
<div class="container">
    <form action="store.php" method="post">

        <strong>Enter your Name: </strong>
        <input type="text" name="name" >
        <br>

        <strong> Enter your Birth date: </strong>
        <input type="date" name="date" >
        <br>

        <input type="submit">

    </form>

</div>
<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

</body>
</html>

