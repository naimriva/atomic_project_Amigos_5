<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Organization Information</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">

</head>
<body>
<div class="container">
    <form action="store.php" method="post">
        <h1>Add Organization Summary </h1>

        <strong>Enter Your Name:</strong>
        <input type="text" name="name" ><br>

        <strong> Enter Organization Name:</strong>
        <input type="text" name="org_name"><br>

        <strong>Enter Organization Summary:</strong>
        <textarea rows="8" cols="60"  name="summary"></textarea>
        <input type="submit">

    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>
</body>
</html>

