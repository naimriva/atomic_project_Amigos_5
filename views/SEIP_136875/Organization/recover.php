<?php
require_once("../../../vendor/autoload.php");

use \App\Organization\Organization;

$objOrg = new Organization();

$objOrg->setData($_GET);

$objOrg->recover();