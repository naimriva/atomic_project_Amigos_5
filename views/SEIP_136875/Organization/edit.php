<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objOrg = new \App\Organization\Organization();
$objOrg->setData($_GET);
$oneData = $objOrg->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Organization Create Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">



    <style>
        body {
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #0D3349;
        }
        p {font-size: 10px;}
        .margin {margin-bottom: 45px;}


        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: #442a8d;
        }
    </style>

</head>
<body>

<div class="container bg-1 text-center">




    <form  class="form-group f" action="update.php" method="post">

        update  Name:
        <input class="form-control" type="text" name="name" value="<?php echo $oneData->name ?>" >
        <br>
        update Organization Name:
        <input class="form-control" type="text" name="org_name" value="<?php echo $oneData->org_name ?>">

        <br>
        update summary:
        <input class="form-control" type="text" name="summary" value="<?php echo $oneData->summary ?>">
        <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

        <input class="btn btn-primary" type="submit" value="Update">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (1000);
        $('#message').fadeIn (1000);
        $('#message').fadeOut (1000);
        $('#message').fadeIn (1000);
        $('#message').fadeOut (1000);
        $('#message').fadeIn (1000);
        $('#message').fadeOut (1000);
    })
</script>



</body>

</html>


