<?php
require_once("../../../vendor/autoload.php");

$objOrg = new \App\Organization\Organization();
$objOrg->setData($_GET);
$oneData = $objOrg->view();

if(isset($_GET['YesButton'])) $objOrg->delete();

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title - Single Book Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>
        body {
            background: antiquewhite;
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #f5f6f7;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}


        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: #442a8d;
        }
        td{
            border: 0px;
            background:antiquewhite;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
            color: black;
        }
    </style>



</head>
<body>


<div class="container bg-1 text-center">
    <h1 style="text-align: center;   color: #442a8d;">Are you sure you want to delete the following record?</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th> Name</th>
            <th>Organization Name</th>
            <th>Summary</th>
        </tr>

        <?php

            echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->org_name</td>
                     <td>$oneData->summary</td>

                  </tr>
              ";

        ?>

    </table>

<?php
   echo "
          <a href='delete.php?id=$oneData->id&YesButton=1' class='btn btn-danger'>Yes</a>

          <a href='index.php' class='btn btn-success'>No</a>
        ";

?>
</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>