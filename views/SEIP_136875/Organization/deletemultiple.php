<?php

require_once("../../../vendor/autoload.php");
use App\Organization\Organization;
use App\Message\Message;
use App\Utility\Utility;
session_start();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title - Single Book Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>
        body {
            background: antiquewhite;
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #000000;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}


        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: black;
        }

        td{
            background: antiquewhite;
            border: 0px;
            color: black;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
            color: black;
        }
    </style>



</head>
<body>



<div class="container bg-1 text-center">



<?php

if(isset($_POST['mark']) || isset($_SESSION['mark'])) {    // start of boss if
   $someData=null;
   $objOrg= new App\Organization\Organization();


   if(isset($_POST['mark']) ){
    $_SESSION['mark'] = $_POST['mark'];
    $someData =  $objOrg->listSelectedData($_SESSION['mark']);
   }
    echo "
          <h1> Are you sure you want to delete all selected data?</h1>
          <a href='deletemultiple.php?YesButton=1' class='btn btn-danger'>Yes</a>
          <a href='index.php' class='btn btn-success'>No</a>
        ";


    if(isset($_GET['YesButton'])){
        $objOrg->deleteMultiple($_SESSION['mark']);
        unset($_SESSION['mark']);
    }
?>


    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>


            <th style='width: 10%; text-align: center'>Serial Number</th>
            <th style='width: 10%; text-align: center'>ID</th>
            <th> Name</th>
            <th>Organization Name</th>
        </tr>

        <?php
        $serial = 1;


        foreach ($someData as $oneData) { ########### Traversing $someData is Required for pagination  #############

            if ($serial % 2) $bgColor = "AZURE";
            else $bgColor = "#ffffff";

            echo "

                  <tr  style='background-color: $bgColor'>


                     <td style='width: 10%; text-align: center'>$serial</td>
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->org_name</td>


                  </tr>
              ";
            $serial++;
        }
        ?>

    </table>

<?php
}  // end of boos if
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect($_SERVER["HTTP_REFERER"]);
}


?>


</div>


</body>
</html>