

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Atomic Project</title>
    <link rel="stylesheet" href="../../resource/bootstrap/css/indexstyle.css">
    <style>
        body {
            background: url("../../resource/bootstrap/image/in1.png");
        }
    </style>
</head>

<body>

<div class="container">
    <div class="container_left">

        <img src="../../resource/bootstrap/image/book.png">
        <a href="BookTitle/index.php"><input type="submit" name="book" VALUE="Book Title"></a><br>

        <img src="../../resource/bootstrap/image/birth.jpg">
        <a href="Birthday/index.php"><input type="submit" name="book" VALUE="Birthday"></a><br>

        <img src="../../resource/bootstrap/image/city.png">
        <a href="City/index.php"><input type="submit" name="book" VALUE="City"></a><br>

        <img src="../../resource/bootstrap/image/email.jpg">
        <a href="Email/index.php"><input type="submit" name="book" VALUE="E-mail"></a>
    </div>


    <div class="container_right">
        <img src="../../resource/bootstrap/image/gender.jpg">
        <a href="Gender/index.php"><input type="submit" name="book" VALUE="Gender"></a><br>
        <img src="../../resource/bootstrap/image/hobby.jpg">
        <a href="Hobby/index.php"><input type="submit" name="book" VALUE="Hobbies"></a><br>
        <img src="../../resource/bootstrap/image/profile.png">
        <a href="ProfilePicture/index.php"><input type="submit" name="book" VALUE="Profile Picture"></a><br>
        <img src="../../resource/bootstrap/image/organization.png">
        <a href="Organization/index.php"><input type="submit" name="book" VALUE="Organization"></a>
    </div>
</div>


</body>
</html>