<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div id='message'> $msg </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ProfilePicture Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">



</head>
<body>


<div class="container">
    <h1 style="color: #442a8d;">Profile Picture Entry</h1>


<form  class="form-group f" action = "store.php" method = "post" enctype="multipart/form-data">
    Please Enter Person's Name:
    <br>
    <input  class="form-control"type="text" name="name">
    <br>
    Enter Person's Profile Picture:
    <input type = "file" name="image" accept=".png, .jpg, .jpeg" >
    <br>
    <input type="submit">
    <br>

</form>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>


</body>
</html>